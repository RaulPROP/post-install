#!/bin/bash

source "$(pwd)/spinner.sh"

# Global variables

tick="\xE2\x9C\x94"
cross="\u274c"

already_installed ()
{
	sleep 0.1
	stop_spinner 1234
}

put_on_hold ()
{
	sleep 0.1
	stop_spinner 5678
}

# $1 is the app name
apt_install ()
{
	if type "$1" >/dev/null 2>&1; then
		start_spinner "Installing $1"
		sleep 0.1
		stop_spinner 1234
	else
		start_spinner "Installing $1"
		sleep 0.1
		apt install $1 -y &>/dev/null &
		stop_spinner "$?"
	fi
}

# $1 is the key, $2 is the deb, $3 is the repo, $4 is the app name
key_and_apt_install ()
{
	if type "$4" >/dev/null 2>&1; then
		start_spinner "Installing $4"
		sleep 0.1
		stop_spinner 1234
	else
		text="Installing $4"
		start_spinner "$text"
		wget -qO - "$1" | sudo apt-key add &>/dev/null
		echo "$2" | sudo tee --append "$3" >/dev/null
		sudo apt update &>/dev/null
		stop_spinner "$?"
		apt_install "$4"
	fi
}

# $1 is the file-name, $2 is the destionation folder
extract_file_to ()
{
	sudo tar -zxvf "${installation_folder}/${1}" -C "${2}" &>/dev/null
}

# $1 is the url, $2 is the file-name
download_file ()
{
	wget -O "${installation_folder}/${2}" "$1" &>/dev/null
}

# $1 is the command
open_external_terminal ()
{
	x-terminal-emulator -e "$1"
}

# $1 is the package url, $2 is the package name, $3 is the app name
install_debian_package ()
{
	text="Installing $3"
	start_spinner "${text}"

	if type "$3" >/dev/null 2>&1; then
		already_installed
	else
		wget "$1" &>/dev/null
		dpkg -i "$2" &>/dev/null
		if [ $? -gt 0 ]; then
			put_on_hold
			start_spinner "${text} dependencies"
			apt-get -f --force-yes --yes install &>/dev/null
			stop_spinner "$?"
			start_spinner "${text}"
			dpkg -i "$2" &>/dev/null
		fi
		stop_spinner "$?"
	fi
}

if [[ $EUID -ne 0 ]]; then
   	start_spinner "Running script as sudo"
	sleep .1
	stop_spinner 1
	exit 1
else

	cmd=(whiptail --separate-output --checklist "Please Select Software you want to install:" 22 76 16)

	echo "Healthcheck 1"

	# any option can be set to default to "on"
	options=(1 "Git" off
	         2 "Curl" off
			 3 "Gparted" off
	         4 "GNOME Tweak-Tools" off
	         5 "VS Codium" off
	         6 "Gnome Shell" off
	         7 "Telegram" off
	         8 "Gitkraken" off
	         9 "Steam" off
	         10 "Chromium" off
	         11 "VLC Media Player" off
	         12 "ZSH" off
	         13 "Node.js" off
	         14 "NPM" off
	         15 "Yarn" off
	         16 "Docker" off
	         17 "Docker Compose" off
			 18 "WebStorm" off
	)

	choices=$("${cmd[@]}" "${options[@]}" 2>&1 >/dev/tty)

	echo "Healthcheck 2"

	if [ ${#choices} -gt 0 ]; then
		# Update
		#start_spinner "Updating packages"
		#apt update &>/dev/null
		#stop_spinner "$?"
		
		# Upgrade
		#start_spinner "Upgrading packages"
		#apt upgrade -y &>/dev/null
		#stop_spinner "$?"

		# Installation folder
		start_spinner "Creating temporal folder"
		mkdir -p $HOME/tmp-post-install
		stop_spinner "$?"
		
		installation_folder=${HOME}/tmp-post-install
		
		for choice in $choices
		do
			case $choice in
				
				1)
					# Git
					apt_install "git"
					;;

				2)
					# Curl
					apt_install "curl"
					;;

				3)
					# Gparted
					apt_install "gparted"
					;;

				4)
					# GNOME Tweak-Tools
					apt_install "gnome-tweak-tool"
					;;

				5)
					# VSCodium
					key="https://gitlab.com/paulcarroty/vscodium-deb-rpm-repo/raw/master/pub.gpg"
					deb="deb https://gitlab.com/paulcarroty/vscodium-deb-rpm-repo/raw/repos/debs/ vscodium main"
					list="/etc/apt/sources.list.d/vscodium.list"
					key_and_apt_install "$key" "$deb" "$list" "vscodium"
					;;

				8)
					# GitKraken
					url="https://release.gitkraken.com/linux/gitkraken-amd64.deb"
					package_name="gitkraken-amd64.deb"
					install_debian_package "$url" "$package_name" "gitkraken"
					;;
				
				12)
					# ZSH
					echo "ZSH: [WIP]"
					;;

				18)
					# WebStorm
					url="https://download-cf.jetbrains.com/webstorm/WebStorm-2019.3.2.tar.gz"
					tar_name="webstorm-installer.tar.gz"

					start_spinner "Preparing to install webstorm"
					download_file "$url" "$tar_name"
					extract_file_to "$tar_name" "/opt"
					webstorm_folder_name=`tar -tzf "${installation_folder}/${tar_name}" | head -1 | cut -f1 -d"/"`
					open_external_terminal "/opt/${webstorm_folder_name}/bin/webstorm.sh"
					stop_spinner "$?"
					;;
				
			esac
		done

		start_spinner "Removing temporal folder"
		sleep 0.1
		#rm -rf "$installation_folder"
		stop_spinner "$?"

		echo "Everything finished."
	else
		echo "¯\_(ツ)_/¯"
	fi
	
fi
