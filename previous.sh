#!/usr/bin/env bash


# update system
if [[ $* == -u ]] || [[ $* == --update ]]; then
	echo "Updating package manager..."
	sudo apt update >/dev/null 2>/dev/null
	echo "Package manager updated."
fi

# Create installation folder
echo "Creating a folder for the Software..."
if [[ -d "${HOME}/Software" ]]; then
	echo "Software folder already exists."
else
	mkdir ~/Software
	echo "Software folder created."
fi

# git
echo "Installing git..."
if type git >/dev/null 2>&1; then
	echo "git already installed."
else
	sudo apt install git -y &>/dev/null
	echo "git installed."
fi

#curl
echo "Intalling curl..."
if type curl >/dev/null 2>&1; then
	echo "curl already installed."
else
	sudo apt install curl -y &>/dev/null
	echo "curl installed."
fi

# install tweak tools
echo "Installing tweak-tools..."
if type gnome-tweaks >/dev/null 2>&1; then
	echo "tweak-tools already installed."
else
	sudo apt-get install gnome-tweak-tool -y >/dev/null
	echo "tweak-tools successfully installed."
fi

# install VS Codium
echo "Installing codium..."
if type codium >/dev/null 2>&1; then
	echo "codium already installed."
else
	wget -qO - https://gitlab.com/paulcarroty/vscodium-deb-rpm-repo/raw/master/pub.gpg | sudo apt-key add &>/dev/null
	echo 'deb https://gitlab.com/paulcarroty/vscodium-deb-rpm-repo/raw/repos/debs/ vscodium main' | sudo tee --append /etc/apt/sources.list.d/vscodium.list >/dev/null
	sudo apt update >/dev/null
	sudo apt install codium -y >/dev/null
	echo "codium successfully installed."
fi

# install gnome extensions
# TODO: ADD FIREFOX PLUGIN
echo "Installing gnome-shell..."
if type gnome-shell >/dev/null 2>&1; then
	echo "gnome-shell already installed."
else
	sudo apt install gnome-shell-extensions -y >/dev/null
	echo "gnome-shell successfully installed."
fi

echo "Installing chrome-gnome-shell..."
if type chrome-gnome-shell >/dev/null 2>&1; then
	echo "chrome-gnome-shell already installed."
else
	sudo apt install chrome-gnome-shell -y >/dev/null
	echo "chrome-gnome-shell successfully installed."
fi

# install title in topbar gnome extension
echo "Installing Title in Topbar extension..."
if [[ -d "/usr/local/share/gnome-shell/extensions/window-title-in-topbar@fthx" ]]; then
	echo "Title in Topbar extension already installed."
else
	wget -O ~/Software/extension.zip "https://extensions.gnome.org/download-extension/window-title-in-topbar@fthx.shell-extension.zip?version_tag=8673" -o /dev/null >/dev/null
	sudo mkdir -p "/usr/local/share/gnome-shell/extensions/window-title-in-topbar@fthx" >/dev/null
	sudo unzip -o ~/Software/extension.zip -d "/usr/local/share/gnome-shell/extensions/window-title-in-topbar@fthx" >/dev/null
	echo "Topbar extension successfully installed."
fi

# install no title bar gnome extension
echo "Installing No Title bar extension..."
if [[ -d "/usr/local/share/gnome-shell/extensions/window-title-in-topbar@fthx" ]]; then
	echo "No Title bar extension already installed"
else
	wget -O ~/Software/extension.zip "https://extensions.gnome.org/download-extension/no-title-bar@franglais125.gmail.com.shell-extension.zip?version_tag=8535" -o /dev/null >/dev/null
	sudo mkdir -p "/usr/local/share/gnome-shell/extensions/no-title-bar@franglais125.gmail.com" >/dev/null
	sudo unzip -o ~/Software/extension.zip -d "/usr/local/share/gnome-shell/extensions/no-title-bar@franglais125.gmail.com" >/dev/null
	echo "No Title bar extension successfully installed."
fi

# another title remove gnome extension
echo "Installing GTK Titlebar extension..."
if [[ -d "/usr/local/share/gnome-shell/extensions/window-title-in-topbar@fthx" ]]; then
	echo "GTK Titlebar extension already installed"
else
	wget -O ~/Software/extension.zip "https://extensions.gnome.org/download-extension/gtktitlebar@velitasali.github.io.shell-extension.zip?version_tag=10713" -o /dev/null >/dev/null
	sudo mkdir -p "/usr/local/share/gnome-shell/extensions/gtktitlebar@velitasali.github.io" >/dev/null
	sudo unzip -o ~/Software/extension.zip -d "/usr/local/share/gnome-shell/extensions/gtktitlebar@velitasali.github.io" >/dev/null
	echo "GTK Titlebar extension successfully installed."
fi

# apply gnome extensions
# gnome-shell --replace &

# remove the last extension zip
rm -rf ~/Software/extension.zip

# Telegram
# TODO: Kill process after finish
echo "Installing telegram-desktop..."
if [[ -d "${HOME}/Software/Telegram" ]]; then
	echo "telegram-desktop already installed."
else
	wget -O ~/Software/telegram.tar.xz "https://telegram.org/dl/desktop/linux" -o /dev/null >/dev/null
	tar -xf ~/Software/telegram.tar.xz -C ~/Software/ >/dev/null
	rm -rf ~/Software/telegram.tar.xz
	sh -c ~/Software/Telegram/Telegram
	echo "telegram-desktop successfully installed."
fi

# Chromium
echo "Installing chrome-gnome-shell..."
if type chromium-browser >/dev/null 2>&1; then
	echo "chromium-browser already installed."
else
	sudo apt install chromium-browser -y >/dev/null
	echo "chromium-browser successfully installed."
fi

# Git Kraken
echo "Installing gitkraken..."
if type gitkraken >/dev/null 2>&1; then
	echo "gitkraken already installed."
else
	wget -O ~/Software/kraken.deb "https://release.gitkraken.com/linux/gitkraken-amd64.deb" -o /dev/null &>/dev/null
	sudo dpkg -i ~/Software/kraken.deb &>/dev/null
	sudo apt-get -f install -y &>/dev/null
	rm -rf ~/Software/kraken.deb
fi

# steam
echo "Installing steam..."
if type stea >/dev/null 2>&1; then
	echo "steam already installed."
else
	sudo apt install steam &>/dev/null
	echo "steam installed."
fi

# whatsapp
echo "Installing google-chrome-stable..."
if type google-chrome-stable >/dev/null 2>&1; then
	echo "google-chrome-stable already installed."
else
	echo "deb [arch=amd64] http://dl.google.com/linux/chrome/deb/ stable main" | sudo tee --append /etc/apt/sources.list.d/google-chrome.list >/dev/null
	wget -qO - https://dl.google.com/linux/linux_signing_key.pub | sudo apt-key add >/dev/null &>/dev/null
	sudo apt update &>/dev/null
	sudo apt install google-chrome-stable -y >/dev/null
	echo "google-chrome-stable successfully installed."
fi
# TODO: Check if whatsapp is installed
# echo "Installing whatsapp..."
# wget -O whatsapp.deb "https://github.com/mawie81/whatsdesktop/releases/download/1.8.0/whatsdesktop_1.8.0_amd64.deb" -o /dev/null
# sudo dpkg -i whatsapp.deb
# rm -rf whatsapp.deb
# echo "whatsapp installed."

# zsh
echo "Installing zsh..."
if type zsh >/dev/null 2>&1; then
	echo "zsh already installed."
else
	sudo apt install zsh -y &>/dev/null
	echo "zsh installed."
	echo "making zsh the default shell..."
	chsh -s $(which zsh)
	echo "you need to restart the computer."
fi

# oh-my-zsh
echo "Installing oh-my-zsh..."
if [[ -d "${HOME}/.oh-my-zsh" ]]; then
	echo "oh-my-zsh already installed."
else
	sh -c "$(wget -O- https://raw.githubusercontent.com/robbyrussell/oh-my-zsh/master/tools/install.sh)" &>/dev/null
	echo "oh-my-zsh installed."
	echo "modifying oh-my-zsh..."
	git clone https://github.com/zsh-users/zsh-autosuggestions ${ZSH_CUSTOM:-~/.oh-my-zsh/custom}/plugins/zsh-autosuggestions &>/dev/null
	sed -i -r 's/\b((ZSH_THEME=").+?[^"])\b/ZSH_THEME="agnoster/' ${HOME}/.zshrc
	sed -i -r 's/\b((plugins=\().+?[^\)])\b/plugins=(git npm yarn dotenv node ng zsh-autosuggestions/' ${HOME}/.zshrc
	echo "DEFAULT_USER=raul" >> ${HOME}/.zshrc
	git clone https://github.com/powerline/fonts.git --depth=1 ${HOME}/Software/fonts &>/dev/null
	${HOME}/Software/fonts/install.sh &>/dev/null
	rm -rf ${HOME}/Software/fonts &>/dev/null
	sudo apt install fonts-powerline -y &>/dev/null
	echo "oh-my-zsh modified."
fi

# nodejs
echo "Installing nodejs..."
if type nodejs >/dev/null 2>&1; then
	echo "nodejs already installed."
else
	sudo apt install nodejs -y &>/dev/null
	echo "nodejs installed."
fi

# npm
echo "Installing npm..."
if type npm >/dev/null 2>&1; then
	echo "npm already installed."
else
	sudo apt install npm -y &>/dev/null
	echo "npm installed."
fi

# solving npm global problem
echo "Solving npm global install problem..."
if [[ -d "${HOME}/.npm-global" ]]; then
	echo "Already solved."
else
	mkdir ${HOME}/.npm-global &>/dev/null
	npm config set prefix '~/.npm-global' &>/dev/null
	export PATH=${HOME}/.npm-global/bin:$PATH &>/dev/null
	echo "export PATH=${HOME}/.npm-global/bin:$PATH" >> ${HOME}/.zshrc
	source ${HOME}/.profile &>/dev/null
	echo "problem solved."
fi

# updating npm
echo "updating npm..."
npm i -g npm@latest &>/dev/null
echo "npm updated."

# updating node
echo "updating node..."
sudo mkdir -p /usr/local/n &>/dev/null
sudo chown -R $(whoami) /usr/local/n &>/dev/null
sudo chown -R $(whoami) /usr/local/bin /usr/local/lib /usr/local/include /usr/local/share &>/dev/null
npm i -g n &>/dev/null
n stable &>/dev/null
echo "node updated."

# installing angular
echo "installing angular..."
npm i -g @angular/cli@latest
echo "angular installed."
echo "installing nest..."
npm i -g @nestjs/cli
echo "nest installed."


# yarn
echo "Installing yarn..."
if type yarn >/dev/null 2>&1; then
	echo "yarn already installed."
else
	curl -sS https://dl.yarnpkg.com/debian/pubkey.gpg | sudo apt-key add - &>/dev/null
	echo "deb https://dl.yarnpkg.com/debian/ stable main" | sudo tee /etc/apt/sources.list.d/yarn.list &>/dev/null
	sudo apt update &>/dev/null
	sudo apt install yarn -y &>/dev/null
	echo "yarn installed."
fi

# Docker
echo "Installing docker..."
if type docker >/dev/null 2>&1; then
	echo "docker already installed."
else
	sudo apt-get install apt-transport-https ca-certificates curl software-properties-common &>/dev/null
	curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add – &>/dev/null
	sudo add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu  $(lsb_release -cs)  stable"  &>/dev/null
	sudo apt-get update &>/dev/null
	sudo apt-get install docker-ce &>/dev/null
	sudo usermod -aG docker ${USER} &>/dev/null
	echo "docker installed. Reboot is needed."
fi

echo "Installing docker-compose"
if type docker-compose >/dev/null 2>&1; then
	echo "docker-compose already installed."
else
	sudo curl -L "https://github.com/docker/compose/releases/download/1.23.2/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose &>/dev/null
	sudo chmod +x /usr/local/bin/docker-compose &>/dev/null
	sudo ln -s /usr/local/bin/docker-compose /usr/bin/docker-compose &>/dev/null
	echo "docker-compose installed."
fi
