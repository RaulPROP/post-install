cycle_states=(
    '●   '
    ' ●  '
    '  ● '
    '   ●'
    '  ● '
    ' ●  '
)

cycle_colors=(
    '\e[91m'
    '\e[92m'
    '\e[93m'
    '\e[94m'
    '\e[95m'
    '\e[96m'
)

len=${#cycle_states[@]}

nc='\e[0m'

i=0

echo $i $len

pos=$(($i % $len))



while [ $i -lt 1000 ];
do
    random=$((RANDOM % 6))
    state="${cycle_states[${pos}]}"
    color="${cycle_colors[${random}]}"
    
    i=$(($i+1%${len}))
    pos=$(($i % $len))

    printf "\b\b\b\b\b\b[${color}${state}${nc}]"

    sleep 0.1
done

printf "\n"